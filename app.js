import express from 'express';
import {} from 'dotenv/config';
import { toDoRoutes } from './routes/route';

const app = express();

app.use(express.json());
app.use("/", toDoRoutes);

const { PORT , HOST } = process.env;

app.listen(PORT , HOST , console.log(`Started at ${PORT} on ${HOST}`));