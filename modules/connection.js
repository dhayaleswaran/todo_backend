import { Sequelize } from 'sequelize';
import { toDoModel } from './toDoModule';

const dbData = {
    DB : 'toDo',
    USERNAME : 'dhaya',
    PASSWORD : 'pass',
    HOST : '127.0.0.1',
    DBTYPE : 'mysql'
}

const connectSequelize = new Sequelize( dbData.DB , dbData.USERNAME , dbData.PASSWORD , {
    host : dbData.HOST,
    dialect : dbData.DBTYPE
});


const toDo = toDoModel(connectSequelize);

// connectSequelize.sync({ force : true });

export { toDo , connectSequelize };