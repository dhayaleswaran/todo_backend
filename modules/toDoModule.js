import { DataTypes } from "sequelize"

const toDoModel = ( con) => {
    return con.define(process.env.TABLETASK, {
        id : {
            type : DataTypes.INTEGER ,
            primaryKey : true,
            autoIncrement : true
        },
        task : DataTypes.STRING
    })
}

export { toDoModel }