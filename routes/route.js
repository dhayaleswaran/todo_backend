import express from 'express';
import { toDo , connectSequelize } from './../modules/connection';

const toDoRoutes = express.Router();

toDoRoutes.post("/addToDoList", async (req , res) => {
    const { task } = req.body;
    const dataToAdd = await toDo.create({ 'task' : task });
    res.json(dataToAdd)
});

toDoRoutes.get("/allToDo", async (req , res) => {
    let tasks = [];
    const viewAll = await toDo.findAll();
        res.json(viewAll)
});

toDoRoutes.put("/modifyData", async ( req , res ) => {
    const { id , content } = req.body;
    const modifyData = await toDo.update({
        'task' : content 
    },{
        where : {
            'id' : id
        }
    }).then((data) => {
        if(data){
            res.json({ status : "Record Modified" })
        } else {
            res.json("Unable to Modify")
        }
    })
       
})

toDoRoutes.delete("/delData", async ( req , res ) => {
    const { id } = req.body;
    const delData = await toDo.destroy({
        where : {
            id : id
        }
    }).then(data => res.json({"Status" : "Deleted" }));
});

export { toDoRoutes };